module.exports = {
	env: {
		"es6": true,
		"node": true,
		"commonjs": true,
		"jest/globals": true,
	},
	extends: ["airbnb-base", "prettier", "plugin:prettier/recommended"],
	plugins: ["jest", "prettier"],
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: "module",
		createDefaultProgram: true,
	},
	overrides: [
		{
			files: "*",
			rules: {
				"no-shadow": "off",
				"no-console": "off",
				"no-debugger": "error",
				"no-underscore-dangle": "off",
				"no-extra-boolean-cast": "off",
				"prettier/prettier": [
					"error",
					{
						singleQuote: false,
						trailingComma: "all",
						endOfLine: "auto",
						bracketSpacing: false,
						arrowParens: "always",
						quotes: "double",
						quoteProps: "consistent",
						semi: true,
						useTabs: true,
						tabWidth: 2,
						printWidth: 120,
					},
				],
			},
		},
	],
};
