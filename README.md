# Holiday Extras User API


A Dockerised and (almost) fully tested user data persistence API and server, powered by Node.JS, Restify and Joi and
returning responses in the JSend industry standard.


## Getting Started


Optionally, you may need to change the default host listening port from `8080` to something that doesn't conflict with
other services already running on your machine. You can do so by changing the value in the `./env` file.


### With Docker


```bash
docker-compose up -d
```


Once the container is running, please follow [this link](http://localhost:8000/user), substituting the port number if
you have changed it in the `./env` file.


### Without Docker


Requirements:

- Node 14


```bash
npm ci
npm start
```


## Saved User Object


```json
{
    "id": 1,
    "email": "joe.bloggs@gmail.com",
    "givenName": "Joseph",
    "familyName": "Bloggs",
    "created": "2021-06-27T14:47:08.149Z",
    "updated": "2021-06-27T14:47:08.149Z"
}
```


## Requests and Responses


### List all Saved Users

---

```http request
GET http://localhost:8000/user
```


### Example Response (200)


```json
{
    "status": "success",
    "data": [
        {
            "id": 1,
            "email": "joe.bloggs@gmail.com",
            "givenName": "Joseph",
            "familyName": "Bloggs",
            "created": "2021-06-27T14:47:08.149Z",
            "updated": "2021-06-27T14:47:08.149Z"
        },
        {
            "id": 2,
            "email": "someone@somewhere.com",
            "givenName": "Someone",
            "familyName": "Missing",
            "created": "2021-06-27T14:49:00.380Z",
            "updated": "2021-06-27T14:49:00.380Z"
        },
        {
            "id": 3,
            "email": "someone-else@somewhere.com",
            "givenName": "Someone",
            "familyName": "Missing",
            "created": "2021-06-27T14:49:00.380Z",
            "updated": "2021-06-27T14:49:00.380Z"
        }
    ]
}
```


### Find Multiple Users By IDs

---

```http request
GET http://localhost:8000/user/1,2,3
```


### Example Successful Response (200)


```json
{
    "status": "success",
    "data": [
        {
            "id": 1,
            "email": "joe.bloggs@gmail.com",
            "givenName": "Joseph",
            "familyName": "Bloggs",
            "created": "2021-06-27T14:47:08.149Z",
            "updated": "2021-06-27T14:47:08.149Z"
        },
        {
            "id": 2,
            "email": "someone@somewhere.com",
            "givenName": "Someone",
            "familyName": "Missing",
            "created": "2021-06-27T14:49:00.380Z",
            "updated": "2021-06-27T14:49:00.380Z"
        },
        {
            "id": 3,
            "email": "someone-else@somewhere.com",
            "givenName": "Someone",
            "familyName": "Missing",
            "created": "2021-06-27T14:49:00.380Z",
            "updated": "2021-06-27T14:49:00.380Z"
        }
    ]
}
```


### Example Bad Request Response (422)


```json
{
    "status": "fail",
    "data": [
        "\"[0]\" must be a number"
    ]
}
```


### Example No Users Found Response (404)


```json
{
    "status": "fail",
    "data": [
        "No users found"
    ]
}
```


### Find Multiple Users By Different Attributes

---

```http request
PUT http://localhost:8000/user
```


### Example Request Body


```json
{
  "emails": [
    "someone@somewhere.com",
    "someone-else@somewhere.com"
  ],
  "givenNames": [
    "Joseph"
  ]
}
```


### Example Successful Response (200)


```json
{
    "status": "success",
    "data": [
        {
            "id": 1,
            "email": "joe.bloggs@gmail.com",
            "givenName": "Joseph",
            "familyName": "Bloggs",
            "created": "2021-06-27T14:47:08.149Z",
            "updated": "2021-06-27T14:47:08.149Z"
        },
        {
            "id": 2,
            "email": "someone@somewhere.com",
            "givenName": "Someone",
            "familyName": "Missing",
            "created": "2021-06-27T14:49:00.380Z",
            "updated": "2021-06-27T14:49:00.380Z"
        },
        {
            "id": 3,
            "email": "someone-else@somewhere.com",
            "givenName": "Someone",
            "familyName": "Missing",
            "created": "2021-06-27T14:49:00.380Z",
            "updated": "2021-06-27T14:49:00.380Z"
        }
    ]
}
```


### Invalid Query Format Example Response (422)


```json
{
    "status": "fail",
    "data": [
        "\"emails[0]\" must be a valid email"
    ]
}
```


### Create a Single User

---

```http request
POST http://localhost:8000/user
```


### Example Request Body

```json
{
    "email": "somebody@somewhere.com",
    "givenName": "Somebody",
    "familyName": "Missing"
}
```


### Example Successful Response (201)


```json
{
    "status": "success",
    "data": {
        "id": 2,
        "email": "somebody@somewhere.com",
        "givenName": "Somebody",
        "familyName": "Missing",
        "updated": "2021-06-27T15:04:45.995Z",
        "created": "2021-06-27T15:04:45.995Z"
    }
}
```


### Duplicate User Example Response (500)

```json
{
    "code": 500,
    "status": "error",
    "message": [
        "email must be unique"
    ]
}
```


### Invalid User Format Example Response (422)


```json
{
    "status": "fail",
    "data": [
        "\"email\" must be a valid email",
        "\"givenName\" is required",
        "\"familyName\" is required"
    ]
}
```


### Create Multiple Users

---


```http request
POST http://localhost:8000/user
```


### Example Request Body


```json
[
    {
        "email": "someone@somewhere.com",
        "givenName": "Someone",
        "familyName": "Missing"
    },
    {
        "email": "someone-else@somewhere.com",
        "givenName": "Someone",
        "familyName": "Missing"
    }
]
```


### Example Successful Response (201)


```json
{
    "status": "success",
    "data": [
        {
            "id": 2,
            "email": "someone@somewhere.com",
            "givenName": "Someone",
            "familyName": "Missing",
            "created": "2021-06-27T15:12:26.218Z",
            "updated": "2021-06-27T15:12:26.218Z"
        },
        {
            "id": 3,
            "email": "someone-else@somewhere.com",
            "givenName": "Someone",
            "familyName": "Missing",
            "created": "2021-06-27T15:12:26.218Z",
            "updated": "2021-06-27T15:12:26.218Z"
        }
    ]
}
```


### Duplicate User Example Response (500)


```json
{
    "code": 500,
    "status": "error",
    "message": [
        "email must be unique"
    ]
}
```


### Invalid User Format Example Response (422)


```json
{
    "status": "fail",
    "data": [
        "\"[0].email\" is required",
        "\"[1].givenName\" is required",
        "\"[1].familyName\" is required"
    ]
}
```


### Update a User

---


```http request
PUT http://localhost:8000/user/1
```


### Example Request Body


```json
{
    "email": "my-other-email@gmail.com"
}
```


### Example Successful Response (200)


```json
{
  "status": "success",
  "data": [
    1
  ]
}
```


### User Not Found Example Response (404)


```json
{
    "status": "fail",
    "data": [
        "User not found"
    ]
}
```


### Delete Users

---


```http request
DELETE http://localhost:8000/user/1,2
```


Multiple users can be deleted at once by comma-separating the user IDs.

No response body will be returned, just a status code of `204` if successful, `404` if not found, otherwise `500`.