const database = require("./database");
require("./models/user");

module.exports = () => {
	return database.sync();
};
