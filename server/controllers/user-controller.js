const Joi = require("joi");
const omit = require("lodash/omit");
const isObject = require("lodash/isObject");
const {Op} = require("sequelize");
const User = require("../models/user");
const DatabaseErrorHandler = require("../utils/database-error-handler");
const userIdSchema = require("../config/validation/multiple-user-ids");
const createManySchema = require("../config/validation/create-user");
const updateUserSchema = require("../config/validation/update-user");
const findManySchema = require("../config/validation/find-many");

module.exports = class UserController {
	static async list(req, res) {
		const users = await User.findAll();
		return res.jsend.success(users);
	}

	static async findManyById(req, res) {
		try {
			const {params} = req;
			const {value, error} = userIdSchema.validate(String(params.ids || "").split(","), {
				stripUnknown: true,
				abortEarly: false,
			});

			if (!!error) {
				const errors = error.details.map(({message}) => message);
				res.status(422);
				return res.jsend.fail(errors);
			}

			const users = await User.findAll({where: {id: {[Op.or]: value}}});

			if (users.length === 0) {
				res.status(404);
				return res.jsend.fail(["No users found"]);
			}

			return res.jsend.success(users);
		} catch (err) {
			return DatabaseErrorHandler(err, res);
		}
	}

	static async findMany(req, res) {
		try {
			const {body} = req;

			if (!isObject(body)) {
				res.status(422);
				return res.jsend.fail(["Payload must be type of Query object"]);
			}

			const {value, error} = findManySchema.validate(body, {stripUnknown: true, abortEarly: false});

			if (!!error) {
				const errors = error.details.map(({message}) => message);
				res.status(422);
				return res.jsend.fail(errors);
			}

			const users = await User.findAll({
				where: {
					[Op.or]: {
						email: {
							[Op.or]: value.emails,
						},
						givenName: {
							[Op.or]: value.givenNames,
						},
						familyName: {
							[Op.or]: value.familyNames,
						},
					},
				},
			});

			if (users.length === 0) {
				res.status(404);
				return res.jsend.fail(["No users found"]);
			}

			return res.jsend.success(users);
		} catch (err) {
			return DatabaseErrorHandler(err, res);
		}
	}

	static async createOneOrMany(req, res) {
		try {
			const {body} = req;

			if (Array.isArray(body)) {
				const {value, error} = Joi.array()
					.items(createManySchema)
					.validate(body, {stripUnknown: true, abortEarly: false});

				if (!!error) {
					const errors = error.details.map(({message}) => message);
					res.status(422);
					return res.jsend.fail(errors);
				}

				const users = await User.bulkCreate(value);
				res.status(201);
				return res.jsend.success(users);
			}

			if (!isObject(body)) {
				res.status(422);
				return res.jsend.fail(["Payload must be type of User object or array of User objects"]);
			}

			const {value, error} = createManySchema.validate(body, {stripUnknown: true, abortEarly: false});

			if (!!error) {
				const errors = error.details.map(({message}) => message);
				res.status(422);
				return res.jsend.fail(errors);
			}

			const user = await User.create(value);
			res.status(201);
			return res.jsend.success(user);
		} catch (err) {
			return DatabaseErrorHandler(err, res);
		}
	}

	static async updateOne(req, res) {
		try {
			const {body, params} = req;

			if (!isObject(body)) {
				res.status(422);
				return res.jsend.fail(["Payload must be type of User object"]);
			}

			const {value, error} = updateUserSchema.validate({...body, ...params}, {stripUnknown: true, abortEarly: false});

			if (!!error) {
				const errors = error.details.map(({message}) => message);
				res.status(422);
				return res.jsend.fail(errors);
			}

			const user = await User.update(omit(value, "id"), {where: {id: value.id}});
			if (user[0] < 1) {
				res.status(404);
				return res.jsend.fail(["User not found"]);
			}

			return res.jsend.success(user);
		} catch (err) {
			return DatabaseErrorHandler(err, res);
		}
	}

	static async deleteMany(req, res) {
		try {
			const {params} = req;
			const {value, error} = userIdSchema.validate(String(params.ids || "").split(","), {
				stripUnknown: true,
				abortEarly: false,
			});

			if (!!error) {
				const errors = error.details.map(({message}) => message);
				res.status(422);
				return res.jsend.fail(errors);
			}

			const count = await User.destroy({where: {id: {[Op.or]: value}}});
			if (count === 0) {
				res.status(404);
				return res.jsend.fail(["No users found"]);
			}

			res.status(204);
			return res.end();
		} catch (err) {
			return DatabaseErrorHandler(err, res);
		}
	}
};
