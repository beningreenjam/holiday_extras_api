const {resolve} = require("path");
const restify = require("restify");
const jsend = require("jsend");
const open = require("open");
const cors = require("cors");
const routes = require("./routes");
const databaseSetup = require("./database-setup");

require("dotenv").config({path: resolve(__dirname, "..", ".env")});

const app = restify.createServer();

app.pre(cors({origin: "http://localhost:3000"}));
app.use(restify.plugins.acceptParser(["application/json"]));
app.use(restify.plugins.bodyParser());
app.use(jsend.middleware);

routes(app);

app.listen(process.env.NODE_ENV === "nodocker" ? process.env.HOST_SERVER_PORT : 8000, async () => {
	process.on("uncaughtException", (err) => {
		console.error(err, "Uncaught Exception thrown");
	});

	await databaseSetup();
	if (process.env.NODE_ENV === "nodocker") {
		open(`http://localhost:${process.env.HOST_SERVER_PORT}/user`);
	}
});

module.exports = app;
