const {Model, DataTypes} = require("sequelize");
const database = require("../database");

class User extends Model {}

User.init(
	{
		email: {
			type: DataTypes.STRING,
			unique: true,
		},
		givenName: {
			type: DataTypes.STRING,
		},
		familyName: {
			type: DataTypes.STRING,
		},
	},
	{
		sequelize: database,
		tableName: "user",
		modelName: "User",
		timestamps: true,
		createdAt: "created",
		updatedAt: "updated",
	},
);

module.exports = User;
