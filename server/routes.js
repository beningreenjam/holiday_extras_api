const UserController = require("./controllers/user-controller");

module.exports = (app) => {
	app.get("/user", UserController.list);
	app.get("/user/:ids", UserController.findManyById);
	app.put("/user", UserController.findMany);
	app.put("/user/:id", UserController.updateOne);
	app.post("/user", UserController.createOneOrMany);
	app.del("/user/:ids", UserController.deleteMany);
};
