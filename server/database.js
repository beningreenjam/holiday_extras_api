const {Sequelize} = require("sequelize");
const {resolve} = require("path");

module.exports = new Sequelize({
	storage: resolve(__dirname, "..", "database.sqlite"),
	dialect: "sqlite",
	logging: false,
	define: {
		freezeTableName: true,
	},
});
