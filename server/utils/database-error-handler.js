const get = require("lodash/get");

module.exports = (err, res) => {
	res.status(500);
	const databaseError = get(err, ["errors", 0, "message"]);
	return res.jsend.error({code: 500, message: [databaseError || err]});
};
