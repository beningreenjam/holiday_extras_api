const Joi = require("joi");

module.exports = Joi.object({
	email: Joi.string().required().email().lowercase(),
	givenName: Joi.string().required(),
	familyName: Joi.string().required(),
});
