const Joi = require("joi");

module.exports = Joi.object({
	id: Joi.number().required().positive().integer(),
	email: Joi.string().email().lowercase(),
	givenName: Joi.string(),
	familyName: Joi.string(),
});
