const Joi = require("joi");

module.exports = Joi.object({
	emails: Joi.array().items(Joi.string().required().email().lowercase()),
	givenNames: Joi.array().items(Joi.string().required()),
	familyNames: Joi.array().items(Joi.string().required()),
});
