const Joi = require("joi");

module.exports = Joi.array().items(Joi.number().required().positive().integer());
