const request = require("supertest");
const omit = require("lodash/omit");
const jsend = require("jsend")({strict: true});
const faker = require("faker");
const {Op} = require("sequelize");
const app = require("../server");
const User = require("../server/models/user");

let users = [];
let datetime;

beforeAll(() => {
	datetime = new Date();
});

beforeEach(() => {
	users = [
		{
			givenName: faker.name.firstName(),
			familyName: faker.name.lastName(),
			email: String(faker.internet.email()).toLowerCase(),
		},
		{
			givenName: faker.name.firstName(),
			familyName: faker.name.lastName(),
			email: String(faker.internet.email()).toLowerCase(),
		},
	];
});

afterEach(async () => {
	await User.destroy({where: {created: {[Op.gte]: datetime}}});
});

afterAll(() => {
	app.close();
});

it("gets the users index endpoint", (done) => {
	request(app)
		.get("/user")
		.expect(200)
		.expect("Content-Type", "application/json")
		.then(async (res) => {
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(res.body.status).toBe("success");
			done();
		});
});

it("lists multiple users", async () => {
	await User.bulkCreate(users);
	await request(app)
		.get("/user")
		.expect(200)
		.then(async (res) => {
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(res.body.data.length).toBeGreaterThanOrEqual(2);
		});
});

it("can create a single user", (done) => {
	const user = users[0];
	request(app)
		.post("/user")
		.send(user)
		.expect(201)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(status).toBe("success");
			expect(data.id).toBeGreaterThan(0);
			expect(data.email).toBe(String(user.email).toLowerCase());
			expect(data.givenName).toBe(user.givenName);
			expect(data.familyName).toBe(user.familyName);
			expect(data.created).toBeDefined();
			expect(data.updated).toBeDefined();
			done();
		});
});

it("can bulk create users", (done) => {
	request(app)
		.post("/user")
		.send(users)
		.expect(201)
		.then(async (res) => {
			const {data} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data.length).toBe(2);

			data.forEach((user) => {
				const userBeforeSave = users.find(({email}) => email === user.email);
				expect(userBeforeSave).toBeDefined();
				expect(user.email).toBe(userBeforeSave.email);
				expect(user.givenName).toBe(userBeforeSave.givenName);
				expect(user.familyName).toBe(userBeforeSave.familyName);
				expect(user.created).toBeDefined();
				expect(user.updated).toBeDefined();
			});

			done();
		});
});

it("must be an object or array of objects", (done) => {
	request(app)
		.post("/user")
		.send("")
		.expect(422)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe("Payload must be type of User object or array of User objects");
			expect(status).toBe("fail");
			done();
		});
});

it("must pass validation for all user objects", (done) => {
	const user = users[0];
	request(app)
		.post("/user")
		.send([user, {}])
		.expect(422)
		.then(async (res) => {
			const {status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(status).toBe("fail");
			done();
		});
});

it("must require an email property", (done) => {
	const user = users[0];
	request(app)
		.post("/user")
		.send(omit(user, "email"))
		.expect(422)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe('"email" is required');
			expect(status).toBe("fail");
			done();
		});
});

it("must require a unique email property", (done) => {
	const user = users[0];
	request(app)
		.post("/user")
		.send([user, user])
		.expect(500)
		.then(async (res) => {
			const {message, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(message[0]).toBe("email must be unique");
			expect(status).toBe("error");
			done();
		});
});

it("must require a valid email property", (done) => {
	const user = users[0];
	request(app)
		.post("/user")
		.send({...omit(user, "email"), email: "cheese"})
		.expect(422)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe('"email" must be a valid email');
			expect(status).toBe("fail");
			done();
		});
});

it("must require a givenName property", (done) => {
	const user = users[0];
	request(app)
		.post("/user")
		.send(omit(user, "givenName"))
		.expect(422)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe('"givenName" is required');
			expect(status).toBe("fail");
			done();
		});
});

it("must require a givenName property of type of string", (done) => {
	const user = users[0];
	request(app)
		.post("/user")
		.send({...omit(user, "givenName"), givenName: []})
		.expect(422)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe('"givenName" must be a string');
			expect(status).toBe("fail");
			done();
		});
});

it("must require a familyName property", (done) => {
	const user = users[0];
	request(app)
		.post("/user")
		.send(omit(user, "familyName"))
		.expect(422)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe('"familyName" is required');
			expect(status).toBe("fail");
			done();
		});
});

it("must require a familyName property of type of string", (done) => {
	const user = users[0];
	request(app)
		.post("/user")
		.send({...omit(user, "familyName"), familyName: []})
		.expect(422)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe('"familyName" must be a string');
			expect(status).toBe("fail");
			done();
		});
});

it("can update a user", async () => {
	const user = await User.create(users[0]);
	const givenName = faker.name.firstName();
	await request(app)
		.put(`/user/${user.id}`)
		.send({givenName})
		.expect(200)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(status).toBe("success");
			expect(data[0]).toBe(1);
		});
});

it("can't update a user unless is an object", async () => {
	const user = await User.create(users[0]);
	await request(app)
		.put(`/user/${user.id}`)
		.send("")
		.expect(422)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe("Payload must be type of User object");
			expect(status).toBe("fail");
		});
});

it("can't update a user without a positive ID", (done) => {
	request(app)
		.put("/user/-1")
		.send({})
		.expect(422)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe('"id" must be a positive number');
			expect(status).toBe("fail");
			done();
		});
});

it("can't update a user without an integer ID", (done) => {
	request(app)
		.put("/user/1.1")
		.send({})
		.expect(422)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe('"id" must be an integer');
			expect(status).toBe("fail");
			done();
		});
});

it("responds with 404 for unknown user ID", (done) => {
	request(app)
		.put("/user/10000")
		.send({})
		.expect(404)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe("User not found");
			expect(status).toBe("fail");
			done();
		});
});

it("fails to update a user to duplicate email address", async () => {
	const savedUsers = await User.bulkCreate(users);
	const user = {
		email: savedUsers[1].email,
	};

	await request(app)
		.put(`/user/${savedUsers[0].id}`)
		.send(user)
		.expect(500)
		.then(async (res) => {
			const {message, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(message[0]).toBe("email must be unique");
			expect(status).toBe("error");
		});
});

it("can find many users by ID", async () => {
	const savedUsers = await User.bulkCreate(users);

	await request(app)
		.get(`/user/${savedUsers[0].id},${savedUsers[1].id}`)
		.expect(200)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data.length).toBe(2);
			expect(status).toBe("success");

			savedUsers.forEach((user) => {
				const userFromResponse = data.find(({id}) => id === user.id);
				expect(userFromResponse).toBeDefined();
				expect(userFromResponse.email).toBe(user.email);
				expect(userFromResponse.givenName).toBe(user.givenName);
				expect(userFromResponse.familyName).toBe(user.familyName);
			});
		});
});

it("fails validation when attempting to find many users by non-numeric ID", async () => {
	const savedUsers = await User.bulkCreate(users);

	await request(app)
		.get(`/user/${savedUsers[0].id},cheese`)
		.expect(422)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe('"[1]" must be a number');
			expect(status).toBe("fail");
		});
});

it("fails to find any users by ID when user IDs don't exist", (done) => {
	request(app)
		.get(`/user/1000,1001`)
		.expect(404)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe("No users found");
			expect(status).toBe("fail");
			done();
		});
});

it("can find many users by various attributes", async () => {
	const savedUsers = await User.bulkCreate(users);
	const queryParams = {
		emails: [savedUsers[0].email],
		givenNames: [savedUsers[1].givenName],
	};

	await request(app)
		.put("/user")
		.send(queryParams)
		.expect(200)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data.length).toBe(2);
			expect(status).toBe("success");

			savedUsers.forEach((user) => {
				const userFromResponse = data.find(({id}) => id === user.id);
				expect(userFromResponse).toBeDefined();
				expect(userFromResponse.email).toBe(user.email);
				expect(userFromResponse.givenName).toBe(user.givenName);
				expect(userFromResponse.familyName).toBe(user.familyName);
			});
		});
});

it("returns 404 when fails to find many users by various attributes", (done) => {
	const queryParams = {
		emails: ["1@1.com"],
		givenNames: ["1"],
		familyNames: ["a"],
	};

	request(app)
		.put("/user")
		.send(queryParams)
		.expect(404)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe("No users found");
			expect(status).toBe("fail");
			done();
		});
});

it("fails to validate when attempting to querying with non-object", (done) => {
	request(app)
		.put("/user")
		.send("")
		.expect(422)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe("Payload must be type of Query object");
			expect(status).toBe("fail");
			done();
		});
});

it("fails to pass validation with invalid query params", (done) => {
	const queryParams = {
		emails: [faker.name.firstName()],
	};

	request(app)
		.put("/user")
		.send(queryParams)
		.expect(422)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe('"emails[0]" must be a valid email');
			expect(status).toBe("fail");
			done();
		});
});

it("can delete multiple users", async () => {
	const savedUsers = await User.bulkCreate(users);
	await request(app).delete(`/user/${savedUsers[0].id},${savedUsers[1].id}`).expect(204);
});

it("fails validation when attempting to delete users using non numeric ID params", (done) => {
	request(app)
		.delete(`/user/cheese`)
		.expect(422)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe('"[0]" must be a number');
			expect(status).toBe("fail");
			done();
		});
});

it("returns 404 when attempting to delete a non-existent user", (done) => {
	request(app)
		.delete(`/user/10000`)
		.expect(404)
		.then(async (res) => {
			const {data, status} = res.body;
			expect(jsend.isValid(res.body)).toBeTruthy();
			expect(data[0]).toBe("No users found");
			expect(status).toBe("fail");
			done();
		});
});
